This repo includes the code to run https://universis.gr

# How to install
  1. Install dependencies

  `npm i`

  2. Build it

  `npm run build`

  3. Run it

  `npm start`

  4. See it

  http://localhost:8000/

# Technologies
  Our webpage is built using [gatsby](https://www.gatsbyjs.org/)
