import React, { Fragment, Component } from "react";
import { Link } from "gatsby";

import Layout from "../components/layout";
import Image from "../components/image";
import SEO from "../components/seo";
import Helmet from "react-helmet";
import Header from "../components/header";
import Characteristics from "../components/characteristics";
import Action from "../components/actions";
import Timeline from "../components/timeline";
import Trial from "../components/trial";
import Universities from "../components/universities";
import Contact from "../components/contact";
import Hero from "../components/hero";
import Footer from "../components/footer";

class IndexPage extends Component {
  state = {
    language: "en"
  };

  changeLanguage = () => {
    let { language } = this.state;
    language = language === "en" ? "gr" : "en";
    this.setState({ language });
  };

  render() {
    const { language } = this.state;

    return (
      <Fragment>
        <SEO
          title="Home"
          keywords={[
            `universis`,
            `sis`,
            `student`,
            `information`,
            `open-source`
          ]}
        />
        <Header lang={language} changeLang={this.changeLanguage} />
        <div className="main-container">
          <Hero lang={language} />
          <Action lang={language} />
          <Timeline lang={language} />
          <Characteristics lang={language} />
          <Trial lang={language} />

          <Universities lang={language} />
          <Contact lang={language} />
          <Footer lang={language} />
        </div>
      </Fragment>
    );
  }
}

export default IndexPage;
