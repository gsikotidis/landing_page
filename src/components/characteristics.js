import { Link } from "gatsby";
import PropTypes from "prop-types";
import React, { Fragment } from "react";
import { CHARACTERISTICS_TEXTS } from "../assets/data/languages";

const Characteristics = props => (
  <Fragment>
    <section className="text-center bg--secondary" id="chars">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-10 col-lg-8">
            <h2>{CHARACTERISTICS_TEXTS.title[props.lang]}</h2>
            <p className="lead">{CHARACTERISTICS_TEXTS.subtitle[props.lang]}</p>
          </div>
        </div>
      </div>
    </section>

    <section className="bg--secondary">
      <div className="container">
        <div className="row">
          <div className="col-md-4">
            <div className="feature feature-4 boxed boxed--lg boxed--border">
              <i className="icon icon-Approved-Window" />
              <h4>{CHARACTERISTICS_TEXTS.design[props.lang]}</h4>
              <hr />
              <p>{CHARACTERISTICS_TEXTS.designSub[props.lang]}</p>
            </div>
          </div>
          <div className="col-md-4">
            <div className="feature feature-4 boxed boxed--lg boxed--border">
              <i className="icon icon-Monitor-phone" />
              <h4>{CHARACTERISTICS_TEXTS.agnostic[props.lang]}</h4>
              <hr />
              <p>{CHARACTERISTICS_TEXTS.agnosticSub[props.lang]}</p>
            </div>
          </div>
          <div className="col-md-4">
            <div className="feature feature-4 boxed boxed--lg boxed--border">
              <i className="icon icon-Plug-In" />
              <h4>{CHARACTERISTICS_TEXTS.flexible[props.lang]}</h4>
              <hr />
              <p>{CHARACTERISTICS_TEXTS.flexibleSub[props.lang]}</p>
            </div>
          </div>
          <div className="col-md-4">
            <div className="feature feature-4 boxed boxed--lg boxed--border">
              <i className="icon icon-Compass" />
              <h4>{CHARACTERISTICS_TEXTS.architecture[props.lang]}</h4>
              <hr />
              <p>{CHARACTERISTICS_TEXTS.architectureSub[props.lang]}</p>
            </div>
          </div>
          <div className="col-md-4">
            <div className="feature feature-4 boxed boxed--lg boxed--border">
              <i className="icon icon-Network" />
              <h4>{CHARACTERISTICS_TEXTS.community[props.lang]}</h4>
              <hr />
              <p>{CHARACTERISTICS_TEXTS.communitySub[props.lang]}</p>
            </div>
          </div>
          <div className="col-md-4">
            <div className="feature feature-4 boxed boxed--lg boxed--border">
              <i className="icon icon-Security-Check" />
              <h4>{CHARACTERISTICS_TEXTS.secure[props.lang]}</h4>
              <hr />
              <p>{CHARACTERISTICS_TEXTS.secureSub[props.lang]}</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  </Fragment>
);

Characteristics.propTypes = {
  siteTitle: PropTypes.string
};

Characteristics.defaultProps = {
  siteTitle: ``
};

export default Characteristics;
