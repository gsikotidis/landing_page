import { Link } from "gatsby";
import PropTypes from "prop-types";
import React, { Fragment } from "react";
import uniStudent from "../assets/images/drawkit-content-man-colour.svg";
import { HERO_TEXTS } from "../assets/data/languages";

const Hero = props => (
  <Fragment>
    <section>
      <div className="container">
        <div className="row align-items-center justify-content-around">
          <div className="col-md-6 col-lg-5">
            <h1 className="head-title">UniverSIS</h1>
            <h2>{HERO_TEXTS.subtitle[props.lang]}</h2>
            <p className="lead">{HERO_TEXTS.info[props.lang]}</p>
            <a className="btn btn--primary type--uppercase" href="#contact">
              <span className="btn__text">{HERO_TEXTS.button[props.lang]}</span>
            </a>
          </div>
          <div className="col-md-6 col-sm-4 text-left">
            <img alt="Image" src={uniStudent} />
          </div>
        </div>
      </div>
    </section>
  </Fragment>
);

export default Hero;
